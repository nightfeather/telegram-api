# TelegramAPI wrapper

## API Method List:

### Updating

  - [x] getUpdates
  - [x] setWebhook
  - [x] deleteWebhook
  - [x] getWebhookInfo

### UserInfo

  - [x] getMe
  - [x] getUserProfilePhotos

### Messaging

  - [x] sendMessage
  - [x] forwardMessage
  - [x] editMessageText
  - [x] editMessageCaption
  - [x] editMessageReplyMarkup
  - [x] deleteMessage
  - ---
  - [x] sendPhoto
  - [x] sendAudio
  - [x] sendDocument
  - [x] sendVideo
  - [x] sendVoice
  - [x] sendVideoNote
  - [x] sendLocation
  - [x] editMessageLiveLocation
  - [x] stopMessageLiveLocation
  - [x] sendVenue
  - [x] sendContact
  - [x] sendChatAction
  - [x] getFile

### Chat

  - [x] kickChatMember
  - [x] unbanChatMember
  - [x] restrictChatMember
  - [x] promoteChatMember
  - [x] exportChatInviteLink
  - [x] setChatPhoto
  - [x] deleteChatPhoto
  - [x] setChatTitle
  - [x] setChatDescription
  - [x] pinChatMessage
  - [x] unpinChatMessage
  - [x] leaveChat
  - [x] getChat
  - [x] getChatAdministrators
  - [x] getChatMembersCount
  - [x] getChatMember
  - [x] setChatStickerSet
  - [x] deleteChatStickerSet
  - [x] answerCallbackQuery

### Sticker

  - [x] sendSticker
  - [x] getStickerSet
  - [x] uploadStickerFile
  - [x] createNewStickerSet
  - [x] addStickerToSet
  - [x] setStickerPositionInSet
  - [x] deleteStickerFromSet

### Payment

  - [x] sendInvoice
  - [x] answerShippingQuery
  - [x] answerPreCheckoutQuery

### Payment

  - [x] sendGame
  - [x] setGameScore
  - [x] getGameHighScores

### Inline

  - [x] answerInlineQuery

## API Type List:

use '&#42;' marks the class an incoming container

### Update

  - [x] Update*
  - [x] WebhookInfo*

### User

  - [x] User*
  - [x] UserProfilePhotos*

### Chat

  - [x] Chat*
  - [x] ChatPhoto*
  - [x] ChatMember*

### Message and embedded entities

  - [x] Message*
  - [x] MessageEntity*
  - [x] PhotoSize*
  - [x] Audio*
  - [x] Document*
  - [x] Video*
  - [x] Voice*
  - [x] VideoNote*
  - [x] Contact*
  - [x] Location*
  - [x] Venue*
  - [x] File*

### ReplayKeyboard

  - [x] ReplyKeyboardMarkup
  - [x] KeyboardButton
  - [x] ReplyKeyboardRemove

### InlineKeyboard

  - [x] CallbackQuery*
  - [x] InlineKeyboardMarkup
  - [x] InlineKeyboardButton
  - [x] ForceReply

### (Not Clear)

  - [x] ResponseParameters

### Sticker

  - [x] Sticker*
  - [x] StickerSet*
  - [x] MaskPosition*

### InlineMode

  - [x] ChosenInlineResult*
  - ---
  - [x] InlineQuery*
  - [x] InlineQueryResult
  - [x] InlineQueryResultCachedAudio
  - [x] InlineQueryResultCachedDocument
  - [x] InlineQueryResultCachedGif
  - [x] InlineQueryResultCachedMpeg4Gif
  - [x] InlineQueryResultCachedPhoto
  - [x] InlineQueryResultCachedSticker
  - [x] InlineQueryResultCachedVideo
  - [x] InlineQueryResultCachedVoice
  - [x] InlineQueryResultArticle
  - [x] InlineQueryResultAudio
  - [x] InlineQueryResultContact
  - [x] InlineQueryResultGame
  - [x] InlineQueryResultDocument
  - [x] InlineQueryResultGif
  - [x] InlineQueryResultLocation
  - [x] InlineQueryResultMpeg4Gif
  - [x] InlineQueryResultPhoto
  - [x] InlineQueryResultVenue
  - [x] InlineQueryResultVideo
  - [x] InlineQueryResultVoice
  - ---
  - [x] InputMessageContent
  - [x] InputTextMessageContent
  - [x] InputLocationMessageContent
  - [x] InputVenueMessageContent
  - [x] InputContactMessageContent

### Payment

  - [x] LabeledPrice
  - [x] ShippingOption
  - [x] Invoice*
  - [x] ShippingAddress*
  - [x] OrderInfo*
  - [x] SuccessfulPayment*
  - [x] ShippingQuery*
  - [x] PreCheckoutQuery*

### Game

  - [x] Game*
  - [x] Animation*
  - [x] GameHighScore*
  - [x] CallbackGame
