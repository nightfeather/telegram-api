def raise(exception : Exception.class, message : String)
  raise exception.new message
end

def raise(exception : Exception.class)
  raise exception.new
end

class URI
  def self.join(*args)
    args.map { |arg| arg.to_s if arg.responds_to?(:to_s) }
        .select { |arg| arg.is_a? String }
        .map { |arg| arg.gsub /^\/|\/$/, "" if arg }
        .join "/"
  end
end
