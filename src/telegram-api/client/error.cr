module TelegramAPI
  class Client
    module Error
      class Base < ::Exception
      end

      class InvalidToken < Base
      end

      class InvalidResponse < Base
      end
    end
  end
end
