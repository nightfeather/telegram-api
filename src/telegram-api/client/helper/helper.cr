module TelegramAPI::Client::Helper
  macro invoke(endpoint, *, klass = nil)
    %resp = HTTP::Client.get make_uri({{endpoint}})
    {% if klass.nil? %}
    parse_response %resp
    {% else %}
    parse_response %resp, {{ klass }}
    {% end %}
  end

  macro invoke(endpoint, params, *, klass = nil)
    %resp = HTTP::Client.post make_uri({{endpoint}}), form: {{params}}
    {% if klass.is_a? NilLiteral %}
    parse_response %resp
    {% else %}
    parse_response %resp, {{ klass }}
    {% end %}
  end

  macro invoke(endpoint, params, headers, *, klass = nil)
    %resp = HTTP::Client.post make_uri({{endpoint}}), {{headers}}, {{params}}
    {% if klass.is_a? NilLiteral %}
      parse_response %resp
    {% else %}
      parse_response %resp, {{ klass }}
    {% end %}
  end

  macro endpoint(name, *args, default = nil, klass = nil, &block)
    def {{name.underscore.id}}({{ *args if args }})
      invoke(
        {{name}},

        {% unless default.is_a? NilLiteral %}
        {{ default }},
        {% end %}

        {% unless klass.is_a? NilLiteral %}
        klass: {{klass}},
        {% end %}
      )
    end

    def {{name.underscore.id}}(
      {% unless (args.is_a? Nop) || args.empty? %}
      {{*args}},
      {% end %}
      **opts)
      opt = {} of String => String
      {% unless default.is_a? NilLiteral %}
        opt.merge! ({{default}})
      {% elsif !args.empty? %}
        opt.merge!({
          {% for arg in args %}
            {{ "#{arg.var}" }} => {{arg.var}}.to_s,
          {% end %}
        })
      {% end %}
      opts.each { |k,v| opt[k.to_s] = v.to_s } unless opts.empty?
      invoke(
        {{name}}, opt,

        {% unless klass.is_a? NilLiteral %}
        klass: {{klass}},
        {% end %}
      )
    end
  end

  # Make a variant for multipart post
  macro endpoint_mp(name, *args, default = nil, klass = nil, &block)
    def {{name.underscore.id}}({{ *args if args }})
      body = parse_multipart(
        {% for arg in args %}
        {{arg.var}}: {{ arg.var.id }},
        {% end %}
      )
      invoke(
        {{name}}, body,

        {% unless klass.is_a? NilLiteral %}
        klass: {{klass}},
        {% end %}
      )
    end

    def {{name.underscore.id}}(
      {% unless (args.is_a? Nop) || args.empty? %}
      {{*args}} ,
      {% end %}
      **opts)
      opt = {} of String => String | Int32 | Int64 | IO
      {% unless default.is_a? NilLiteral %}
        opt.merge! ({{default}})
      {% elsif !args.empty? %}
        opt.merge!({
          {% for arg in args %}
            {{ "#{arg.var}" }} => {{arg.var}},
          {% end %}
        })
      {% end %}
      opts.each { |k,v| opt[k.to_s] = v } unless opts.empty?
      req = parse_multipart(opt)

      invoke(
        {{name}}, req[:buffer], HTTP::Headers{ "Content-Type" => req[:content_type] },

        {% unless klass.is_a? NilLiteral %}
        klass: {{klass}},
        {% end %}
      )
    end
  end

  def parse_multipart(**params)
    parse_multipart(params.to_h)
  end

  def parse_multipart(params : Hash(_, _))
    buffer = IO::Memory.new
    content_type = ""
    HTTP::FormData.build io: buffer do |g|
      content_type = g.content_type
      params.each do |key, val|
        unless val.is_a? File
          g.field key.to_s, val.to_s
        else
          g.file key.to_s, val,
            HTTP::FormData::FileMetadata.new(filename: val.path),
            HTTP::Headers{"Content-Type" => "application/octet-stream"}
        end
      end
    end
    {buffer: buffer.rewind, content_type: content_type}
  end
end
