require "http/client"
require "http/formdata"
require "uri"
require "json"
require "./error.cr"
require "./endpoints"

module TelegramAPI
  class Client
    BaseURI = "https://api.telegram.org/bot"

    include Endpoints

    @token : String
    @baseuri : String

    def self.start(token : String, &block : Client -> _)
      yield new token
    end

    def initialize(@token : String)
      @baseuri = BaseURI + @token + "/"
      validate_token!(@token)
    end

    private def make_uri(part : String | URI)
      uri = URI.parse(URI.join(@baseuri, part))
      debug_msg(uri)
      uri
    end

    private def parse_response(response : HTTP::Client::Response)
      begin
        data = JSON.parse response.body
      rescue
        raise Error::InvalidResponse, response.body
      end
      {code: response.status_code, data: data}
    end

    private def parse_response(response : HTTP::Client::Response, klass : Class)
      begin
        data = klass.from_json response.body, "result"
      rescue
        raise Error::InvalidResponse, "unexpeceted response```\n" + response.body + "\n```"
      end
      {code: response.status_code, data: data}
    end

    private def validate_token!(token : String)
      unless token =~ /^\d+:\w+$/
        raise(Error::InvalidToken)
      end
    end

    macro debug_msg(msg)
      puts {{msg}} if ENV["DEBUG"]?
    end
  end
end
