module TelegramAPI
  class Client
    module Endpoints
      # Sticker

      endpoint("sendSticker", chat_id : ChatID, sticker : String, klass: Type::Message)
      endpoint_mp("sendSticker", chat_id : ChatID, sticker : File, klass: Type::Message)

      endpoint("getStickerSet", name : String, klass: Type::StickerSet)

      endpoint("uploadStickerFile", user_id : Int64, png_sticker : String, klass: Type::File)
      endpoint_mp("uploadStickerFile", user_id : Int64, png_sticker : File, klass: Type::File)

      endpoint("createNewStickerSet", user_id : Int64, name : String, title : String,
        png_sticker : String, emojis : String, klass: Bool)

      endpoint_mp("createNewStickerSet", user_id : Int64, name : String, title : String,
        png_sticker : File, emojis : String, klass: Bool)

      endpoint("addStickerToSet", user_id : Int64, name : String, png_sticker : String, emojis : String, klass: Bool)
      endpoint_mp("addStickerToSet", user_id : Int64, name : String, png_sticker : File, emojis : String, klass: Bool)

      endpoint("setStickerPositionInSet", sticker : String, position : Int64, klass: Bool)

      endpoint("deleteStickerFromSet", sticker : String, klass: Bool)
    end
  end
end
