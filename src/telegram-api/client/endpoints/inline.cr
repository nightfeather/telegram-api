module TelegramAPI
  class Client
    module Endpoints
      endpoint("answerInlineQuery", inline_query_id : String,
        results : Array(InlineQueryResult), klass: Bool)
    end
  end
end
