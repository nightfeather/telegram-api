module TelegramAPI
  class Client
    module Endpoints
      # Invoice

      endpoint("sendInvoice", chat_id : ChatID,
        title : String, description : String,
        payload : String, provider_token : String,
        start_parameter : String,
        currency : String, prices : Array(LabeledPrice),
        klass: Type::Message)

      endpoint("answerShippingQuery", shipping_query_id : String,
        ok : Bool, klass: Bool)

      endpoint("answerPreCheckoutQuery", pre_checkout_query_id : String,
        ok : Bool, klass: Bool)
    end
  end
end
