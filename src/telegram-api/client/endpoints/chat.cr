module TelegramAPI
  class Client
    module Endpoints
      # Chat

      {% for act in %w{kick unban restrict promote} %}
      endpoint({{ act + "ChatMember" }}, chat_id : ChatID, user_id : Int, klass: Bool)
      {% end %}

      endpoint("exportChatInviteLink", chat_id : ChatID, klass: String)

      endpoint("setChatPhoto", chat_id : ChatID, photo : String, klass: Bool)
      endpoint_mp("setChatPhoto", chat_id : ChatID, photo : File, klass: Bool)

      endpoint("deleteChatPhoto", chat_id : ChatID, klass: Bool)

      endpoint("setChatTitle", chat_id : ChatID, title : String, klass: Bool)
      endpoint("setChatDescription", chat_id : ChatID, description : String, klass: Bool)

      endpoint("pinChatMessage", chat_id : ChatID, message_id : Int, klass: Bool)

      endpoint("unpinChatMessage", chat_id : ChatID, klass: Bool)

      endpoint("leaveChat", chat_id : ChatID, klass: Bool)

      endpoint("getChat", chat_id : ChatID, klass: Type::Chat)

      endpoint("getChatAdministrators", chat_id : ChatID, klass: Array(Type::ChatMember))

      endpoint("getChatMemebersCount", chat_id : ChatID, klass: Int64)

      endpoint("getChatMemeber", chat_id : ChatID, user_id : Int, klass: Type::ChatMember)

      endpoint("setChatStickerSet", chat_id : ChatID, sticker_set_name : String, klass: Bool)

      endpoint("deleteChatStickerSet", chat_id : ChatID, klass: Bool)
    end
  end
end
