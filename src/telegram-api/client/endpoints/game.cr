module TelegramAPI
  class Client
    module Endpoints
      # Game

      endpoint("sendGame", chat_id : ChatID,
        game_short_name : String, klass: Type::Message)

      endpoint("setGameScore", chat_id : ChatID,
        message_id : Int64, user_id : Int64, score : Int64, klass: Bool)

      endpoint("setGameScore", inline_message_id : Int64, user_id : Int64,
        score : Int64, klass: Bool)

      endpoint("getGameHighScores", chat_id : ChatID, message_id : Int64,
        user_id : Int64, klass: Array(Type::GameHighScore))

      endpoint("getGameHighScores", inline_message_id : Int64,
        user_id : Int64, klass: Array(Type::GameHighScore))
    end
  end
end
