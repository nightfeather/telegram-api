module TelegramAPI
  class Client
    module Endpoints
      # Message General

      endpoint("sendMessage", chat_id : ChatID, text : String, klass: Type::Message)

      endpoint("editMessageText", chat_id : ChatID, message_id : Int64,
        text : String, klass : Union(Bool, Type::Message))

      endpoint("editMessageText", inline_message_id : Int64,
        text : String, klass : Union(Bool, Type::Message))

      endpoint("editMessageCaption", chat_id : ChatID, message_id : Int64,
        caption : String, klass: Union(Bool, Type::Message))

      endpoint("editMessageCaption", inline_message_id : Int64,
        caption : String, klass: Union(Bool, Type::Message))

      endpoint("editMessageReplyMarkup", chat_id : ChatID, message_id : Int64,
        reply_markup : String, klass: Union(Bool, Type::Message))

      endpoint("editMessageReplyMarkup", inline_message_id : Int64,
        reply_markup : String, klass: Union(Bool, Type::Message))

      endpoint("deleteMessage", chat_id : ChatID,
        message_id : Int64, klass: Bool)

      endpoint "forwardMessage", from_chat_id : ChatID, chat_id : ChatID,
        message_id : Int, klass: Type::Message

      endpoint("editMessageLiveLocation", chat_id : ChatID, message_id : Int64,
        latitude : Float, longitude : Float,
        klass: Union(Bool, Type::Message))

      endpoint("editMessageLiveLocation", inline_message_id : Int64,
        latitude : Float, longitude : Float,
        klass: Union(Bool, Type::Message))

      endpoint("stopMessageLiveLocation", chat_id : ChatID,
        message_id : Int64, klass: Union(Bool, Type::Message))

      endpoint("stopMessageLiveLocation", inline_message_id : Int64,
        klass: Union(Bool, Type::Message))

      # Attachment

      {% for ftype in %w{Audio Document Photo Video Voice VideoNote} %}

        endpoint {{ "send" + ftype }}, chat_id : ChatID,
                  {{ftype.underscore.id}} : String, klass: Type::Message
        endpoint_mp {{ "send" + ftype }}, chat_id : ChatID,
                  {{ ftype.underscore.id }} : File, klass: Type::Message

      {% end %}

      endpoint("sendLocation", chat_id : ChatID,
        latitude : Float, longitude : Float, klass: Type::Message)

      endpoint("sendVenue", chat_id : ChatID,
        latitude : Float, longitude : Float,
        name : String, address : String, klass: Type::Message)

      endpoint("sendContact", chat_id : ChatID,
        phone_number : String, first_name : String, klass: Type::Message)

      endpoint("sendChatAction", chat_id : ChatID,
        action : String, klass: Bool)

      endpoint("getFile", file_id : String, klass: Type::File)
    end
  end
end
