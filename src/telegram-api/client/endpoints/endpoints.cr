require "../helper"

module TelegramAPI
  class Client
    module Endpoints
      include Helper

      alias ChatID = Int32 | Int64 | String

      endpoint("getUpdates", klass: Array(Type::Update))

      endpoint("setWebhook", url : String, klass: Bool)
      endpoint_mp("setWebhook", url : String, certificate : File, klass: Bool)

      endpoint("deleteWebhook", klass: Bool)

      endpoint("getWebhookInfo", klass: Type::WebhookInfo)

      endpoint("getMe", klass: Type::User)

      endpoint("getUserProfilePhotos", user_id : Int, klass: Type::UserProfilePhotos)
    end
  end
end

require "./**"
