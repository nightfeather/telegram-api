module TelegramAPI
  module Type
    class UserProfilePhotos < Base
      JSON.mapping(
        total_count: Int64,
        photos: Array(Array(PhotoSize))
      )
    end
  end
end
