module TelegramAPI
  module Type
    class Chat < Base
      JSON.mapping(
        id: Int64,
        "type": String,
        title: String?,
        username: String?,
        first_name: String?,
        last_name: String?,
        all_members_are_administrators: Bool?,
        photo: ChatPhoto?,
        description: String?,
        invite_link: String?,
        pinned_message: Message?
      )

      {% for t in %w{private group supergroup channel} %}
      def {{ ("is_" + t + "?").id }}
        @type == {{ t }}
      end
      {% end %}
    end
  end
end
