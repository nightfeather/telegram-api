module TelegramAPI
  module Type
    class ResponseParameters
      JSON.mapping(
        migrate_to_chat_id: Int64,
        retry_after: Int64
      )
    end
  end
end
