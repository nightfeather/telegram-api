module TelegramAPI
  module Type
    class WebhookInfo < Base
      JSON.mapping(
        url: String,
        has_custom_certificate: Bool,
        pending_update_count: Int64,
        last_error_date: Int64?,
        last_error_message: String?,
        max_connections: Int64?,
        allowed_updates: Array(String)?
      )

      def has_custom_certificate?
        @has_custom_certificate
      end

      def has_error?
        @last_error_date || @last_error_message
      end
    end
  end
end
