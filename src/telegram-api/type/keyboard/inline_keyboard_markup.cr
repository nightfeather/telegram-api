module TelegramAPI
  module Type
    class InlineKeyboardMarkup < ReplyMarkup
      JSON.mapping(
        inline_keyboard: Array(Array(InlineKeyboardButton))
      )
    end
  end
end
