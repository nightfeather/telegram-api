module TelegramAPI
  module Type
    class ReplyKeyboardMarkup < ReplyMarkup
      JSON.mapping(
        keyboard: Array(Array(KeyboardButton)),
        resize_keyboard: Bool?,
        one_time_keyboard: Bool?,
        selective: Bool?
      )
    end
  end
end
