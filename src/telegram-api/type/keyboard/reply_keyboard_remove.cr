module TelegramAPI
  module Type
    class ReplyKeyboardRemove < ReplyMarkup
      JSON.mapping(
        remove_keyboard: {"type": Bool, default: true, "setter": false},
        selective: Bool?
      )

      def initialize
        @remove_keyboard = true
      end
    end
  end
end
