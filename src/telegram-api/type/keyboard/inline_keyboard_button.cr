module TelegramAPI
  module Type
    class InlineKeyboardButton < Base
      JSON.mapping(
        text: String,
        url: String?,
        callback_data: String?,
        switch_inline_query: String?,
        switch_inline_query_current_chat: String?,
        callback_game: CallbackGame?,
        pay: Bool?
      )
    end
  end
end
