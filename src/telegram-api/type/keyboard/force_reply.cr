module TelegramAPI
  module Type
    class ForceReply < ReplyMarkup
      JSON.mapping(
        force_reply: {"type": Bool, default: true, "setter": false},
        selective: Bool?
      )

      def initialize
        @force_reply = true
      end
    end
  end
end
