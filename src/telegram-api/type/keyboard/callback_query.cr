module TelegramAPI
  module Type
    class CallbackQuery < Base
      JSON.mapping(
        id: String,
        from: User,
        message: Message?,
        inline_message_id: String?,
        chat_instance: String,
        data: String?,
        game_short_name: String?
      )

      def is_inline?
        not @inline_message_id.nil?
      end

    end
  end
end
