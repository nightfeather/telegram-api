module TelegramAPI
  module Type
    class KeyboardButton < Base
      JSON.mapping(
        text: String,
        request_contact: Bool?,
        request_location: Bool?
      )
    end
  end
end
