module TelegramAPI
  module Type
    class Base
      def initialize(**params)
        params.each { |k, v|
          send "#{k}=", v if responds_to? :"#{k}="
        }
      end
    end
  end
end
