module TelegramAPI
  module Type
    class Update < Base
      JSON.mapping(
        update_id: Int64,
        message: Message?,
        edited_message: Message?,
        channel_post: Message?,
        edited_channel_post: Message?,
        inline_query: InlineQuery?,
        chosen_inline_result: ChosenInlineResult?,
        callback_query: CallbackQuery?,
        shipping_query: ShippingQuery?,
        pre_checkout_query: PreCheckoutQuery?
      )

      def type
        if @message
          :message
        elsif @edited_message
          :edited_message
        elsif @channel_post
          :channel_post
        elsif @edited_channel_post
          :edited_channel_post
        elsif @inline_query
          :inline_query
        elsif @chosen_inline_result
          :chosen_inline_result
        elsif @callback_query
          :callback_query
        elsif @shipping_query
          :shipping_query
        elsif @pre_checkout_query
          :pre_checkout_query
        else
          :crap
        end
      end
    end
  end
end
