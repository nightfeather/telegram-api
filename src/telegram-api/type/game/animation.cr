module TelegramAPI
  module Type
    class Animation < Base
      JSON.mapping(
        file_id: String,
        thumb: PhotoSize?,
        file_name: String?,
        mime_type: String?,
        file_size: Int64?
      )
    end
  end
end
