module TelegramAPI
  module Type
    class GameHighScore < Base
      JSON.mapping(
        position: Int64,
        user: User,
        score: Int64
      )
    end
  end
end
