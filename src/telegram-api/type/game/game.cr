module TelegramAPI
  module Type
    class Game < Base
      JSON.mapping(
        title: String,
        description: String,
        photo: Array(PhotoSize),
        text: String?,
        text_entities: Array(MessageEntity)?,
        animation: Animation?
      )
    end
  end
end
