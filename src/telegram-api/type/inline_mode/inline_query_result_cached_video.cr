module TelegramAPI
  module Type
    class InlineQueryResultCachedVideo < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        video_file_id: String,
        title: String,
        caption: String?,
        description: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @video_file_id)
        super("video")
      end
    end
  end
end
