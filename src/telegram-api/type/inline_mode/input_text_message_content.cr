module TelegramAPI
  module Type
    class InputTextMessageContent < InputMessageContent
      JSON.mapping(
        message_text: String,
        parse_mode: String?,
        disable_web_page_preview: Bool?
      )

      def initialize(@message_text)
      end
    end
  end
end
