module TelegramAPI
  module Type
    class InlineQueryResultLocation < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        latitude: Float64,
        longitude: Float64,
        title: String,
        live_period: Int64?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?,
        thumb_url: String?,
        thumb_width: Int64?,
        thumb_height: Int64?
      )

      def initialize(@title, @latitude, @longitude)
        super("location")
      end
    end
  end
end
