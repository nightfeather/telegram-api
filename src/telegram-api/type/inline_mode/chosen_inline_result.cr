module TelegramAPI
  module Type
    class ChosenInlineResult < Base
      JSON.mapping(
        result_id: String,
        from: User,
        location: Location?,
        inline_message_id: String?,
        query: String
      )
    end
  end
end
