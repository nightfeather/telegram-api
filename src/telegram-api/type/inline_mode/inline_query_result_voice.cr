module TelegramAPI
  module Type
    class InlineQueryResultVoice < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        voice_url: String,
        title: String,
        caption: String?,
        voice_duration: Int64?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @voice_url)
        super("voice")
      end
    end
  end
end
