module TelegramAPI
  module Type
    class InlineQueryResultVenue < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        latitude: Float64,
        longitude: Float64,
        title: String,
        address: String,
        foursquare_id: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?,
        thumb_url: String?,
        thumb_width: Int64?,
        thumb_height: Int64?
      )

      def initialize(@title, @latitude, @longitude, @address)
        super("venue")
      end
    end
  end
end
