module TelegramAPI
  module Type
    class InlineQueryResultCachedGif < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        gif_file_id: String,
        title: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @gif_file_id)
        super("gif")
      end
    end
  end
end
