module TelegramAPI
  module Type
    class InlineQueryResultCachedAudio < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        audio_file_id: String,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@audio_file_id)
        super("audio")
      end
    end
  end
end
