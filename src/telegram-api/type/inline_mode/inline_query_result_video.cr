module TelegramAPI
  module Type
    class InlineQueryResultVideo < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        video_url: String,
        mime_type: String,
        thumb_url: String,
        title: String,
        caption: String?,
        video_width: Int64?,
        video_height: Int64?,
        video_duration: Int64?,
        description: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @video_url, @thumb_url, @mime_type)
        super("video")
      end
    end
  end
end
