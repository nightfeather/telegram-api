module TelegramAPI
  module Type
    class InputVenueMessageContent < InputMessageContent
      JSON.mapping(
        latitude: Float64,
        longitude: Float64,
        title: String,
        address: String,
        foursquare_id: String?,
      )

      def initialize(@title, @latitude, @longitude, @address)
      end
    end
  end
end
