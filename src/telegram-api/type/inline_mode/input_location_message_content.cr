module TelegramAPI
  module Type
    class InputLocationMessageContent < InputMessageContent
      JSON.mapping(
        latitude: Float64,
        longitude: Float64,
        live_period: Int64?
      )
    end

    def initialize(@latitude, @longitude)
    end
  end
end
