module TelegramAPI
  module Type
    class InlineQueryResult < Base
      # inheritance relation control class, cannot be used directly

      @id : String
      @type : String

      def initialize(@type)
        @id = make_id
      end

      private def make_id(length : Int = 20)
        Random::Secure.urlsafe_base64(length)
      end
    end
  end
end

require "random/secure"
