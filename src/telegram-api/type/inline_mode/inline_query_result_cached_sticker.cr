module TelegramAPI
  module Type
    class InlineQueryResultCachedSticker < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        sticker_file_id: String,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@sticker_file_id)
        super("sticker")
      end
    end
  end
end
