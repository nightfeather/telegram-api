module TelegramAPI
  module Type
    class InlineQueryResultContact < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        phone_number: String,
        first_name: String,
        last_name: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?,
        thumb_url: String?,
        thumb_width: Int64?,
        thumb_height: Int64?
      )

      def initialize(@phone_number, @first_name)
        super("contact")
      end
    end
  end
end
