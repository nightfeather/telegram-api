module TelegramAPI
  module Type
    class InlineQueryResultMpeg4Gif < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        mpeg4_url: String,
        mpeg4_width: Int64?,
        mpeg4_height: Int64?,
        mpeg4_duration: Int64?,
        thumb_url: String,
        title: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@mpeg4_url, @thumb_url)
        super("mpeg4_gif")
      end
    end
  end
end
