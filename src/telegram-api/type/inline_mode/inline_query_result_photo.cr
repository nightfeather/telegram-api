module TelegramAPI
  module Type
    class InlineQueryResultPhoto < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        photo_url: String,
        thumb_url: String,
        photo_width: Int64?,
        photo_height: Int64?,
        title: String?,
        description: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@photo_url, @thumb_url)
        super("photo")
      end
    end
  end
end
