module TelegramAPI
  module Type
    class InlineQueryResultDocument < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        title: String,
        caption: String?,
        document_url: String,
        mime_type: String,
        description: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?,
        thumb_url: String?,
        thumb_width: Int64?,
        thumb_height: Int64?
      )

      def initialize(@title, @document_url, @mime_type)
        super("document")
      end
    end
  end
end
