module TelegramAPI
  module Type
    class InlineQuery < Base
      JSON.mapping(
        id: String,
        from: User,
        location: Location?,
        query: String,
        offset: String
      )
    end
  end
end
