module TelegramAPI
  module Type
    class InputContactMessageContent < InputMessageContent
      JSON.mapping(
        phone_number: String,
        first_name: String,
        last_name: String?
      )
    end

    def initialize(@phone_number, @first_name)
    end
  end
end
