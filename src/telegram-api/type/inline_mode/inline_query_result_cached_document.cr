module TelegramAPI
  module Type
    class InlineQueryResultCachedDocument < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        title: String,
        caption: String?,
        document_file_id: String,
        description: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @document_file_id)
        super("document")
      end
    end
  end
end
