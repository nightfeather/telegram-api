module TelegramAPI
  module Type
    class InlineQueryResultGif < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        gif_url: String,
        gif_width: Int64?,
        gif_height: Int64?,
        gif_duration: Int64?,
        thumb_url: String,
        title: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@gif_url, @thumb_url)
        super("gif")
      end
    end
  end
end
