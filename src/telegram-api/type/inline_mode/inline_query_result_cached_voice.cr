module TelegramAPI
  module Type
    class InlineQueryResultCachedVoice < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        voice_file_id: String,
        title: String,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@title, @voice_file_id)
        super("voice")
      end
    end
  end
end
