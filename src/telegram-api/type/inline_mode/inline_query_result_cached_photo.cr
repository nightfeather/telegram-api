module TelegramAPI
  module Type
    class InlineQueryResultCachedPhoto < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        photo_file_id: String,
        title: String?,
        description: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@photo_file_id)
        super("photo")
      end
    end
  end
end
