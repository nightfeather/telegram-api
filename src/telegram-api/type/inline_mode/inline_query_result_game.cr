module TelegramAPI
  module Type
    class InlineQueryResultGame < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        game_short_name: String,
        reply_markup: InlineKeyboardMarkup?
      )

      def initialize(@game_short_name)
        super("game")
      end
    end
  end
end
