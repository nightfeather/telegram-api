module TelegramAPI
  module Type
    class InlineQueryResultArticle < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        title: String,
        input_message_content: InputMessageContent,
        reply_markup: InlineKeyboardMarkup?,
        url: String?,
        hide_url: Bool?,
        description: String?,
        thumb_url: String?,
        thumb_width: Int64?,
        thumb_height: Int64?
      )

      def initialize(@title, @input_message_content)
        super("article")
      end

      def initialize(title, text : String)
        initialize(title, InputMessageTextContent.new text)
      end

      def initialize(title, contact : Contact)
        initialize(title, InputContactMessageContent.new contact)
      end

      def initialize(title, location : Location)
        initialize(title, InputLocationMessageContent.new location)
      end

      def initialize(title, venue : Venue)
        initialize(title, InputVenueMessageContent.new venue)
      end
    end
  end
end
