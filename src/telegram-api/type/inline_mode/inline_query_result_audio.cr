module TelegramAPI
  module Type
    class InlineQueryResultAudio < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        audio_url: String,
        title: String,
        caption: String?,
        performer: String?,
        audio_duration: Int64?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@audio_url, @title)
        super("audio")
      end
    end
  end
end
