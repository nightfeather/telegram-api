module TelegramAPI
  module Type
    class InlineQueryResultCachedMpeg4Gif < InlineQueryResult
      JSON.mapping(
        "type": {"type": String, "setter": false},
        id: {type: String, setter: false},
        mpeg4_file_id: String,
        title: String?,
        caption: String?,
        reply_markup: InlineKeyboardMarkup?,
        input_message_content: InputMessageContent?
      )

      def initialize(@mpeg4_file_id)
        super("mpeg4_gif")
      end
    end
  end
end
