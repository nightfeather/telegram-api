module TelegramAPI
  module Type
    class Voice < Base
      JSON.mapping(
        file_id: String,
        duration: Int64,
        mime_type: String?,
        file_size: Int64?
      )
    end
  end
end
