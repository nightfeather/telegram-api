module TelegramAPI
  module Type
    class File < Base
      JSON.mapping(
        file_id: String,
        file_size: Int64?,
        file_path: String?
      )
    end
  end
end
