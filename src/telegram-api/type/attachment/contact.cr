module TelegramAPI
  module Type
    class Contact < Base
      JSON.mapping(
        phone_number: String,
        first_name: String,
        last_name: String?,
        user_id: Int64?
      )
    end
  end
end
