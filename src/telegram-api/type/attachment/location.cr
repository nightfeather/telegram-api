module TelegramAPI
  module Type
    class Location < Base
      JSON.mapping(
        longitude: Float64,
        latitude: Float64
      )
    end
  end
end
