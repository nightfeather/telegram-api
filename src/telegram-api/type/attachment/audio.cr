module TelegramAPI
  module Type
    class Audio < Base
      JSON.mapping(
        file_id: String,
        duration: Int64,
        performer: String?,
        title: String?,
        mime_type: String?,
        file_size: Int64?
      )
    end
  end
end
