module TelegramAPI
  module Type
    class Video < Base
      JSON.mapping(
        file_id: String,
        width: Int64,
        height: Int64,
        duration: Int64,
        thumb: PhotoSize?,
        mime_type: String?,
        file_size: Int64?
      )
    end
  end
end
