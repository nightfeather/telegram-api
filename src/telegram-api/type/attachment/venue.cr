module TelegramAPI
  module Type
    class Venue < Base
      JSON.mapping(
        location: Location,
        title: String,
        address: String,
        foursquare_id: String?
      )
    end
  end
end
