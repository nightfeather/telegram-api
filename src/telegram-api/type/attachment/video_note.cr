module TelegramAPI
  module Type
    class VideoNote < Base
      JSON.mapping(
        file_id: String,
        length: Int64,
        duration: Int64,
        thumb: PhotoSize?,
        file_size: Int64?
      )
    end
  end
end
