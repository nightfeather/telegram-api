module TelegramAPI
  module Type
    class Message < Base
      JSON.mapping(
        id: {"type": Int64, "key": "message_id"},
        from: User?,
        date: {"type": Time, "converter": Time::EpochConverter},
        chat: Chat,
        forward_from: User?,
        forward_from_chat: Chat?,
        forward_from_message_id: Int64?,
        forward_signature: String?,
        forward_date: {"type": Time?, "converter": Time::EpochConverter},
        reply_to_message: Message?,
        edit_date: {"type": Time?, "converter": Time::EpochConverter},
        author_signature: String?,
        text: String?,
        entities: Array(MessageEntity)?,
        audio: Audio?,
        document: Document?,
        game: Game?,
        photo: Array(PhotoSize)?,
        sticker: Sticker?,
        video: Video?,
        voice: Voice?,
        video_note: VideoNote?,
        caption: String?,
        contact: Contact?,
        location: Location?,
        venue: Venue?,
        new_chat_members: Array(User)?,
        left_chat_member: User?,
        new_chat_title: String?,
        new_chat_photo: Array(PhotoSize)?,
        delete_chat_photo: Bool?,
        group_chat_created: Bool?,
        supergroup_chat_created: Bool?,
        channel_chat_created: Bool?,
        migrate_to_chat_id: Int64?,
        migrate_from_chat_id: Int64?,
        pinned_message: Message?,
        invoice: Invoice?,
        successful_payment: SuccessfulPayment?
      )

      def type
        if @text
          :text
        elsif @audio
          :audio
        elsif @document
          :document
        elsif @game
          :game
        elsif @photo
          :photo
        elsif @sticker
          :sticker
        elsif @video
          :video
        elsif @voice
          :voice
        elsif @video_note
          :video_note
        elsif @caption
          :caption
        elsif @contact
          :contact
        elsif @location
          :location
        elsif @venue
          :venue
        elsif @new_chat_members
          :new_char_member
        elsif @left_chat_member
          :left_chat_member
        else
          :crap
        end
      end

      {% for t in %w( text audio document game photo sticker video voice video_note caption contact location venue new_char_member left_chat_member ) %}
      def {{ ("is_" + t + "?").id }}
        self.type == {{ t.id }}
      end
      {% end %}

      def is_forwarded?
        not @forward_from_message_id.nil?
      end

      def is_reply?
        not @reply_to_message.nil?
      end

      def is_edited?
        not @edit_date.nil?
      end
    end
  end
end
