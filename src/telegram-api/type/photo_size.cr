module TelegramAPI
  module Type
    class PhotoSize < Base
      JSON.mapping(
        file_id: String,
        width: Int64,
        height: Int64,
        file_size: Int64?
      )
    end
  end
end
