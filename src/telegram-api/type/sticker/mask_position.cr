module TelegramAPI
  module Type
    class MaskPosition < Base
      JSON.mapping(
        point: String,
        x_shift: Float64,
        y_shift: Float64,
        scale: Float64
      )
    end
  end
end
