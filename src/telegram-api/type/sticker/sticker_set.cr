module TelegramAPI
  module Type
    class StickerSet < Base
      JSON.mapping(
        name: String,
        title: String,
        contains_masks: Bool,
        stickers: Array(Sticker)
      )
    end
  end
end
