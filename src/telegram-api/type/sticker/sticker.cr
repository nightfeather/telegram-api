module TelegramAPI
  module Type
    class Sticker < Base
      JSON.mapping(
        file_id: String,
        width: Int64,
        height: Int64,
        thumb: PhotoSize?,
        emoji: String?,
        set_name: String?,
        mask_position: MaskPosition?,
        file_size: Int64?
      )
    end
  end
end
