module TelegramAPI
  module Type
    class MessageEntity < Base
      JSON.mapping(
        "type": String, # Can be mention (@username), hashtag, bot_command, url, email, bold (bold text), italic (italic text), code (monowidth string), pre (monowidth block), text_link (for clickable text URLs), text_mention (for users without usernames)
        "offset": Int64,
        "length": Int64,
        "url": String?,
        "user": User?
      )
      {% for t in %w{mention hashtag bot_command url email bold italic code pre text_link text_mention} %}
      def {{ ("is_" + t + "?").id }}
        @type == {{ t }}
      end
      {% end %}
    end
  end
end
