module TelegramAPI
  module Type
    class User < Base
      JSON.mapping(
        id: Int64,
        is_bot: Bool,
        first_name: String,
        last_name: String?,
        username: String?,
        language_code: String?
      )

      def is_bot?
        @is_bot
      end

      def has_username?
        not @username.nil?
      end
    end
  end
end
