module TelegramAPI
  module Type
    class Invoice < Base
      JSON.mapping(
        title: String,
        description: String,
        start_parameter: String,
        currency: String,
        total_amount: Int64
      )
    end
  end
end
