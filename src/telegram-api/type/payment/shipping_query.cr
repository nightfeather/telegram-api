module TelegramAPI
  module Type
    class ShippingQuery < Base
      JSON.mapping(
        id: String,
        from: User,
        invoice_payload: String,
        shipping_address: ShippingAddress
      )
    end
  end
end
