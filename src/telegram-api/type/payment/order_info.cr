module TelegramAPI
  module Type
    class OrderInfo < Base
      JSON.mapping(
        name: String?,
        phone_number: String?,
        email: String?,
        shipping_address: ShippingAddress?
      )
    end
  end
end
