module TelegramAPI
  module Type
    class ShippingOption < Base
      JSON.mapping(
        id: String,
        title: String,
        prices: Array(LabeledPrice)
      )
    end
  end
end
