module TelegramAPI
  module Type
    class LabeledPrice < Base
      JSON.mapping(
        label: String,
        amount: Int64
      )
    end
  end
end
