module TelegramAPI
  module Type
    class SuccessfulPayment < Base
      JSON.mapping(
        currency: String,
        total_amount: Int64,
        invoice_payload: String,
        shipping_option_id: String,
        order_info: OrderInfo,
        telegram_payment_charge_id: String,
        provider_payment_charge_id: String
      )
    end
  end
end
