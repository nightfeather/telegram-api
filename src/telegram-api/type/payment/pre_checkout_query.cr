module TelegramAPI
  module Type
    class PreCheckoutQuery < Base
      JSON.mapping(
        id: String,
        from: User,
        currency: String,
        total_amount: Int64,
        invoice_payload: String,
        shipping_option_id: String?,
        order_info: OrderInfo?
      )
    end
  end
end
